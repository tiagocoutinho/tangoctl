
History
=======

0.2.3 (2018-11-25)
------------------

* Update homepage
* Remove garbage files


0.2.2 (2018-11-25)
------------------

* Minor fixes to HISTORY


0.2.1 (2018-11-25)
------------------

* Minor fixes to README


0.2.0 (2018-11-25)
------------------

* Release with major device and server commands


0.1.0 (2018-11-17)
------------------

* First release on PyPI.
